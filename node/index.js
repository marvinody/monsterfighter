//Code from Manny
const Devilbat = require('./Monsters/Devilbat'),
Cerberos = require('./Monsters/Cerberos'),
status = require('./Status')
monstersConstructors = [Devilbat, Cerberos];


const rng = {
  seed: 1,
  int : function(l=0, h=10000){
    return Math.floor(
      (h-l)*this.rand() + l
    );
  },
  //range of [0,1)
  rand: function(){//https://stackoverflow.com/questions/521295
    let x = Math.sin(this.seed++) * 10000;
    return x - Math.floor(x);
  },
}

const rotate = arr => arr.push(arr.shift());

const attack = (atk, def) => {
  const max = atk.numMoves();
  let idx = 0;
  //while heads and we haven't capped
  while (rng.int(0,2) > 0 && idx < max)
    idx += 1;
  //constrain
  const move_idx = Math.min(max-1, idx);
  return atk.doMove(move_idx, def);
};
const num_fights = parseInt(process.argv[2],10) || 100000,
results = monstersConstructors.map(()=>0),
starting_seed = 646523;

let starting_idx = 0;

//holds who an array symbolizing a count of won at a
//certain turn count represented by its index
let turn_cnt_array = [];

//turn count idx -> number of instagibs at that turn count
let instagib_cnt = []

for(let j=0;j<2;j++){
  rng.seed = starting_seed;
  for(let i=0;i<num_fights/2;i++){
    //console.log(`--------FIGHT--------`);
    const monsters = monstersConstructors.map(m => new m());

    //determines who starts attacking first
    let idx = starting_idx;
    //just niceties, prev prevents negative indices also
    const next_idx = () => (idx+1)%monsters.length
    const prev_idx = () => ((idx-1)%monsters.length+monsters.length)%monsters.length

    let turn_cnt = 0,
      high_turn_attempt_seed = rng.seed;

    while(monsters[idx].canFight()){
      let attacking = monsters[idx];
      let def_idx = next_idx();
      let defending = monsters[def_idx];

      atk_status = attack(attacking, defending);

      switch(atk_status){
        case status.TURN_DONE:
          //do nothing cause we don't need to
          break;
        case status.TURN_EXTRA:
          //roll back index by one
          //console.log(`EXTRA TURN FOR ${attacking.colorName()}`)
          idx = prev_idx();
          break;


      }
      idx = next_idx();
      turn_cnt += 1;
    }

    let loser = monsters[idx],
    winner = monsters[prev_idx()];
    results[prev_idx()] += 1;


    if(turn_cnt_array[turn_cnt] == undefined){
      turn_cnt_array[turn_cnt] = Array(monsters.length).fill(0);
      instagib_cnt[turn_cnt] = 0;
    }

    turn_cnt_array[turn_cnt][prev_idx()] += 1;

    if(atk_status == status.INSTAGIBBED){
      instagib_cnt[turn_cnt] += 1;
    }
    //console.log(`${winner.colorName()} wins with ${winner.hp} hp and ${loser.colorName()} dies with ${loser.hp} hp`);

  }
  starting_idx = 1
}

console.log(`Devilbat: ${results[0]}`);
console.log(`Cerberos: ${results[1]}`);
//console.log(results);
//turn_cnt_array.forEach((cnt, i)=>console.log(`Length: ${i}  Times:${cnt} Instagibbed: ${instagib_cnt[i]}`))
//console.log(`Total Instagibs:${instagib_cnt.reduce((acc,e)=>e+acc,0)}`)
