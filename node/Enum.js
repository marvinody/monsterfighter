module.exports = s => {
  return Object.freeze(
    //take a string and enums are space delimited
    s.split(' ').map(st => st.trim())
    //then make symbols for each one and return as object
    .reduce((acc,x) => {acc[x] = Symbol(x); return acc},{})
  )//finally, freeze everything
}
