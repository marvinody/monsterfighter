const Monster = require('../Monster'),
status = require('../Status')
module.exports = class Devilbat extends Monster {
  constructor(){
    super();
    this.hp = 10;
    this.color = "\x1b[34m";

    this.moves.push(this.wingChop);
    this.moves.push(this.fire);
    this.moves.push(this.hypnoticEyeBeam);
    this.moves.push(this.bloodsucking);
  }
  wingChop(e){
    e.hp -= 1;
  }
  fire(e){
    e.hp -= 2;
  }
  hypnoticEyeBeam(e){
    e.hp -= 2;
    return status.TURN_EXTRA;
  }
  bloodsucking(e){
    e.hp -= 4;
    this.hp += 4;
  }

}
