const Monster = require('../Monster'),
status = require('../Status')
module.exports = class Cerberos extends Monster {
  constructor(){
    super();
    this.hp = 12;
    this.color = "\x1b[31m";

    this.moves.push(this.failed);
    this.moves.push(this.bite);
    this.moves.push(this.bite);
    this.moves.push(this.claw);
    this.moves.push(this.swallow);
  }
  failed(e){
    return;
  }
  bite(e){
    e.hp -= 3;
  }
  claw(e){
    e.hp -= 5;
  }
  swallow(e){
    e.hp -= e.hp;
    return status.INSTAGIBBED;
  }

}
