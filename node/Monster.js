const status = require('./Status');

//https://stackoverflow.com/questions/9781218
const reset_color = "\x1b[0m";

module.exports = class Monster{
  constructor(){
    this.moves = [];
    this.hp = 0;
    this.color = "\x1b[36m"
  }
  name(){
    return this.constructor.name;
  }
  colorName(){
    return `${this.color}${this.name()}${reset_color}`
  }
  numMoves(){
    return this.moves.length;
  }

  canFight(){
    return this.hp > 0;
  }

  doMove(idx, enemy){

    idx = Math.min(idx, this.moves.length);
    //console.log(`${this.colorName()} Attacks ${enemy.colorName()} with ${this.moves[idx].name}`)
    return this.moves[idx].call(this, enemy) || status.TURN_DONE;
  }

}
