public class Cerberus extends Monster{

	public Cerberus(){
		hp = 12;
	}

	public void retard(Monster enemy){
		return;
	}

	public void bite(Monster enemy){
		enemy.hp = enemy.hp - 3;
	}

	public void claw(Monster enemy){
		enemy.hp = enemy.hp - 5;
	}

	public void instagib(Monster enemy){
		enemy.hp = 0;
	}

	@Override
	public void action(Monster enemy){
		if (this.hp<=0||enemy.hp<=0){
			return;
		}
		switch (roll()){
			case 0: retard(enemy); break;
			case 1:
			case 2: bite(enemy); break;
			case 3: claw(enemy); break;
			default:
			case 4: instagib(enemy); break;
		}
	}
}
