public abstract class Monster {
	int hp;

	public int roll(){
		int headCount = 0;
		while(randBool()) {
			headCount++;
		}
		return headCount;
	}

	public boolean randBool(){
		return Math.random()<0.5;
	}

	public abstract void action(Monster enemy);

}
