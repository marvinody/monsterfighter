//Code from Vinnie
public class Fight {
	public static void main(String [] args){

		int sampleSize = 100000;
		try {
			sampleSize = Integer.parseInt(args[0]);
		} catch (Exception e){

		}
		int dbWin = 0;
		int cWin = 0;
		for (int i = 0 ; i < sampleSize/2; i ++ ){
			Monster devilbat = new Devilbat();
			Monster cerberus = new Cerberus();

			while(devilbat.hp>0 && cerberus.hp>0){
				devilbat.action(cerberus);
				cerberus.action(devilbat);
			}

			if(checkWinner(devilbat,cerberus)){
				dbWin ++;
			} else {
				cWin ++;
			}
		}
		for (int i = 0 ; i < sampleSize/2; i ++ ){
			Monster devilbat = new Devilbat();
			Monster cerberus = new Cerberus();

			while(devilbat.hp>0 && cerberus.hp>0){
				cerberus.action(devilbat);
				devilbat.action(cerberus);
			}

			if(checkWinner(devilbat,cerberus)){
				dbWin ++;
			} else {
				cWin ++;
			}
		}
		System.out.println("Devilbat: " + dbWin);
		System.out.println("Cerberus: " + cWin);
	}

	public static boolean checkWinner(Monster a, Monster b){
		return b.hp<=0;
	}
}
