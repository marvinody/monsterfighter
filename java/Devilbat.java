public class Devilbat extends Monster{
	public Devilbat(){
		hp = 10;
	}

	public void wingChop(Monster enemy){
		enemy.hp = enemy.hp - 1;
	}

	public void fire(Monster enemy){
		enemy.hp = enemy.hp - 2;
	}

	public void eyebeam(Monster enemy){
		enemy.hp = enemy.hp - 2;
		action(enemy);
	}

	public void succ(Monster enemy){
		enemy.hp = enemy.hp - 4;
		this.hp = this.hp + 4;
	}

	@Override
	public void action(Monster enemy){
		if (this.hp<=0||enemy.hp<=0){
			return;
		}
		switch (roll()){
			case 0: wingChop(enemy); break;
			case 1: fire(enemy); break;
			case 2: eyebeam(enemy); break;
			default:
			case 3: succ(enemy); break;
		}
	}
}
