TRIALS=100000
echo "Executing $TRIALS trials"

echo "\nNodeJS"
node node/index.js $TRIALS

echo "\nPython3.5"
python3.5 python/main.py $TRIALS

echo "\nJava"
cd java
javac Fight.java && java Fight $TRIALS
cd ../
