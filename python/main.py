# Code from George
import sys
import random
import itertools

devilwins = 0
cerbwins = 0
totalgamesplayed = 100000
try:
	totalgamesplayed = int(sys.argv[0])
except:
	pass
gamesplayed = 0

#return some number between 0-4
def get_consecutive_heads():
	flip = None
	total = 0
	while flip != 0:
		flip = random.randint(0, 1)
		total += flip
		if total >= 4:
			break
	return total

while gamesplayed < totalgamesplayed:
	devilhp = 10
	cerbhp = 12
	turn = random.randint(0,1) #0 = dev 1 = cerb
	while cerbhp > 0 and devilhp > 0:
		move = get_consecutive_heads()

		if turn == 0:
			if move == 0:
				cerbhp -= 1
			elif move == 1:
				cerbhp -= 2
			elif move == 2:
				cerbhp -= 2
				turn += 1
			elif move >= 3:
				cerbhp -= 4
				devilhp += 4
		else:
			if move == 0:
				pass
			elif move == 1:
				devilhp -= 3
			elif move == 2:
				devilhp -= 3
			elif move == 3:
				devilhp -= 5
			elif move == 4:
				devilhp = 0
		turn = (turn + 1) % 2

	if cerbhp <= 0:
		devilwins += 1
	else:
		cerbwins += 1
	gamesplayed += 1
	if gamesplayed % 100000 == 0:
		print("Devilbat: {}\nCerberos: {}".format(devilwins, cerbwins))
